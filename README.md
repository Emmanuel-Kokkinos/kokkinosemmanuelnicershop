I used the Nexus 5X API 29 for my project.

For this second assignment, I fixed all of the mistakes of my first assignment. 
For starters, I downloaded git so I was able to do my project on here. 
I fixed my formatting problem in my manifest but I got another problem. I was adding up navigation like I did in assignment 1 but this time it isn't showing on my menu page.
I believe it is because I was using a basic activity instead of an empty one and in the manifest page it has android:theme="@style/AppTheme.NoActionBar". I think that is the problem
but if I remove that line my app no longer works.
I fixed my checkout activity to show the checkout totals and taxes but because of other problems with this assignment, I wasn't able to update it to use the underlying data so I 
still did it with values from views.
I added logs this time to my code.
I added saveInstanceState for the menu activity because it is the only activity that needs it but I only saved the state of my bulbasaur card because of other problems in this 
assignment I will get to. 
For the cardview as root element, I am still having trouble with that. I think I was doing it right but it was making my app crash so I commented it out and put it within a 
LinearLayout again.
Finally, for corrections from my first assignment, I have a license file and a proper readme file.

Now for new problems with this assignment:
Like already mentioned, the cardview was not my root element.
My big problems came with the recyclerview. For everything I did, I think the idea is right but something wasn't working so I never got to see if any of it worked.
I made my model package and the java class for it and I was going to make an array of objects but I stopped at 1 because I couldn't get it to work but I understand how to make 9 
others if I was doing it right.
I was following a codelab to help me make the recyclerview but because it wasn't working I commented it all out. All the code I was trying to use is all in the files.
My alert dialog I think should be good but it doesn't show my list of options for delivery. The "cancel" and "checkout" buttons are there and both work but the list isn't. Again,
I have the code in the file that I thought should have worked.
For menus, there is an options menu which is the one I used. An options menu is a menu on the app bar that you can add items to to do different things. I only made a simple menu
that displayed toast messages when clicked but I could have had a button to go to the menu activity for example. 
Another menu type that I didn't use is a context menu. A context menu would appear when you did something like long press a button. It is used to do things for that button that
you long clicked.
One final problem I had was that I couldn't figure out how to send the data from more than one textview to the checkout menu so I could only get it to work with the first cardview.
So my checkout menu for my app will only show the cost and taxes of buying bulbasaurs.