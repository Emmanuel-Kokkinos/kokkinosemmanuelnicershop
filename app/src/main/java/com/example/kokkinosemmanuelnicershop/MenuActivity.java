package com.example.kokkinosemmanuelnicershop;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import com.example.kokkinosemmanuelnicershop.model.ShoppingItems;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.util.LinkedList;

public class MenuActivity extends AppCompatActivity {

    //private final LinkedList<ShoppingItems> shoppingItems = new LinkedList<>();
    //private RecyclerView recyclerView;
    //private ListAdapter adapter;
    private int bulbasaurQuantity = 0;
    private int charmanderQuantity = 0;
    private int squirtleQuantity = 0;
    private TextView showBulbQuantity;
    private TextView showCharQuantity;
    private TextView showSquirtQuantity;
    private TextView showBulbTotal;
    private TextView showCharTotal;
    private TextView showSquirtTotal;
    public static final String EXTRA_MESSAGE = "com.example.android.kokkinosemmanuelnicershop.extra.MESSAGE";
    private TextView totalPrice;
    private double delivery;
    private static final String TAG = "MenuActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        showBulbQuantity = (TextView) findViewById(R.id.bulbasaur_quantity);
        showCharQuantity = (TextView) findViewById(R.id.charmander_quantity);
        showSquirtQuantity = (TextView) findViewById(R.id.squirtle_quantity);
        showBulbTotal = (TextView) findViewById(R.id.bulbasaur_total);
        showCharTotal = (TextView) findViewById(R.id.charmander_total);
        showSquirtTotal = (TextView) findViewById(R.id.squirtle_total);

        // Checks if it needs to restore values that would have been lost
        if(savedInstanceState != null) {
            showBulbTotal.setText(savedInstanceState.getString("bulbasaur_total"));
            showBulbQuantity.setText(savedInstanceState.getString("bulbasaur_quantity"));
        }

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickShowAlert(view);
            }
        });

        totalPrice = findViewById(R.id.bulbasaur_total);

        Log.d(TAG, "Creating menu activity");
        //I would have made 10 items in my array if I could get it to work
//        shoppingItems.addLast(new ShoppingItems(getResources().getString(R.string.bulbasaur_title),
//                getResources().getString(R.string.bulbasaur_desc),
//                Double.parseDouble(getResources().getString(R.string.bulbasaur_price)),
//                "@drawable/bulbasaur", Integer.parseInt(getResources().getString(R.string.bulbasaur_quantity))));

//        recyclerView = findViewById(R.id.recyclerview);
//        adapter = new ListAdapter(this, shoppingItems);
//        recyclerView.setAdapter(adapter);
//        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    /**
     * Saves state if phone is rotated
     * @param outState
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if(findViewById(R.id.bulbasaur_total) != null) {
            outState.putString("bulbasaur_total", showBulbTotal.getText().toString());
            outState.putString("bulbasaur_quantity", showBulbQuantity.getText().toString());
        }
        Log.d(TAG, "save instance state");
    }

    /**
     * Button click subtracts 1 bulbasaur and updates total
     */
    public void bulbasaurSubtract(View view) {
        if (bulbasaurQuantity > 0) {
            bulbasaurQuantity--;
            showBulbQuantity.setText(Integer.toString(bulbasaurQuantity));
            Double total = bulbasaurQuantity * 3.50;
            showBulbTotal.setText(String.format("%.2f", total));
        }
        Log.d(TAG, "bulbasaur subtract");
    }

    /**
     * Button click adds 1 bulbasaur and updates total
     */
    public void bulbasaurAdd(View view) {
        bulbasaurQuantity++;
        showBulbQuantity.setText(Integer.toString(bulbasaurQuantity));
        Double total = bulbasaurQuantity * 3.50;
        showBulbTotal.setText(String.format("%.2f", total));
        Log.d(TAG, "bulbasaur add");
    }

    /**
     * Button click subtracts 1 charmander and updates total
     */
    public void charmanderSubtract(View view) {
        if (charmanderQuantity > 0) {
            charmanderQuantity--;
            showCharQuantity.setText(Integer.toString(charmanderQuantity));
            Double total = charmanderQuantity * 9.99;
            showCharTotal.setText(String.format("%.2f", total));
        }
        Log.d(TAG, "charmander subtract");
    }

    /**
     * Button click adds 1 charmander and updates total
     */
    public void charmanderAdd(View view) {
        charmanderQuantity++;
        showCharQuantity.setText(Integer.toString(charmanderQuantity));
        Double total = charmanderQuantity * 9.99;
        showCharTotal.setText(String.format("%.2f", total));
        Log.d(TAG, "charmander add");
    }

    /**
     * Button click subtracts 1 squirtle and updates total
     */
    public void squirtleSubtract(View view) {
        if (squirtleQuantity > 0) {
            squirtleQuantity--;
            showSquirtQuantity.setText(Integer.toString(squirtleQuantity));
            Double total = squirtleQuantity * 8.88;
            showSquirtTotal.setText(String.format("%.2f", total));
        }
        Log.d(TAG, "squirtle subtract");
    }

    /**
     * Button click adds 1 squirtle and updates total
     */
    public void squirtleAdd(View view) {
        squirtleQuantity++;
        showSquirtQuantity.setText(Integer.toString(squirtleQuantity));
        Double total = squirtleQuantity * 8.88;
        showSquirtTotal.setText(String.format("%.2f", total));
        Log.d(TAG, "squirtle add");
    }

    /**
     * Shows a popup dialog box to choose which shipping option is wanted and to go to checkout activity
     * @param view
     */
    public void onClickShowAlert(final View view) {
        String[] deliveryOptions = getResources().getStringArray(R.array.deliveryOptions);
        AlertDialog.Builder myAlertBuilder = new AlertDialog.Builder(MenuActivity.this);
        myAlertBuilder.setTitle(getString(R.string.alert_title));
        myAlertBuilder.setMessage(getString(R.string.alert_message));
        myAlertBuilder.setSingleChoiceItems(deliveryOptions, 2, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                switch (i) {
                    case 0:
                        delivery = 50;
                        break;
                    case 1:
                        delivery = 10;
                        break;
                    case 2:
                        delivery = 0;
                        break;
                }
                dialogInterface.dismiss();
            }
        });

        //Goes to checkout activity
        myAlertBuilder.setPositiveButton("Checkout", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                checkoutActivity(view);
            }
        });
        myAlertBuilder.setNegativeButton("Cancel", null);
        AlertDialog dialog = myAlertBuilder.create();
        dialog.show();
        Log.d(TAG, "use alert dialog");
    }

    /**
     * Button click opens checkout activity
     */
    public void checkoutActivity(View view) {
        Intent intent = new Intent(MenuActivity.this, CheckoutActivity.class);
        String message = totalPrice.getText().toString();
        intent.putExtra(EXTRA_MESSAGE, message);
        startActivity(intent);
        Log.d(TAG, "Going to checkout activity");
    }
}
