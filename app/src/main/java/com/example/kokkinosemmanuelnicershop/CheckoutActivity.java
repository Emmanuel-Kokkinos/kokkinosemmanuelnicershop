package com.example.kokkinosemmanuelnicershop;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

public class CheckoutActivity extends AppCompatActivity {

    private static final String TAG = "CheckoutActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);
        Intent intent = getIntent();
        String message = intent.getStringExtra(MenuActivity.EXTRA_MESSAGE);
        TextView beforeTax = findViewById(R.id.total_before_tax);
        TextView tvq = findViewById(R.id.tvq_tax);
        TextView tps = findViewById(R.id.tps_tax);
        TextView afterTax = findViewById(R.id.total_after_tax);
        beforeTax.setText(message);
        double d = Double.parseDouble(message);
        double tvq_tax = d * .09975;
        double tps_tax = d * .05;
        tvq.setText(String.format("%.2f", tvq_tax));
        tps.setText(String.format("%.2f", tps_tax));
        afterTax.setText(String.format("%.2f", d + tvq_tax + tps_tax));
        Log.d(TAG, "Create checkout activity");
    }
}
