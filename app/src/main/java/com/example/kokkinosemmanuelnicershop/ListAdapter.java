package com.example.kokkinosemmanuelnicershop;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.kokkinosemmanuelnicershop.model.ShoppingItems;

import java.util.LinkedList;

//public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ItemViewHolder> {
//
//    private final LinkedList<ShoppingItems> itemList;
//    private LayoutInflater inflater;
//
//    public ShoppingItems getItem(int position) {
//        return itemList != null ? itemList.get(position) : null;
//    }
//
//    @NonNull
//    @Override
//    public ListAdapter.ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//        View itemView = inflater.inflate(R.layout.card1, parent, false);
//        return new ItemViewHolder(itemView, this);
//    }
//
//    public ListAdapter(Context context, LinkedList<ShoppingItems> itemList) {
//        inflater = LayoutInflater.from(context);
//        this.itemList = itemList;
//    }
//
//    @Override
//    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
//        ShoppingItems current = itemList.get(position);
//        holder.item = getItem(position);
//    }
//
//    @Override
//    public int getItemCount() {
//        return itemList.size();
//    }
//
//    class ItemViewHolder extends RecyclerView.ViewHolder {
//        final ListAdapter adapter;
//        private ShoppingItems item;
//
//        public ItemViewHolder(View itemView, ListAdapter adapter) {
//            super(itemView);
//            this.adapter = adapter;
//        }
//    }
//}
